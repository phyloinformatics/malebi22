# Machine Learning for Bioinformatics, Fall 2022

This project contains some of the intructor's notes, slide decks, and notebooks (Google Colab) for the graduate course of "Machine Learning for Bioinformatics" (BINF6210 and BINF8210, combine) as given in the Fall semester of 2022 at the [Department of Bioinformatics and Genomics](https://cci.charlotte.edu/bioinformatics/47/3) (BiG), [College of Computing and Informatics](https://cci.charlotte.edu/) (CCI), [University of North Carlina at Charlotte](https://www.charlotte.edu/) (UNC Charlotte). The instructor for this course was [Dr. Denis Jacob Machado](https://phyloinformatics.com/members/Denis_Jacob_Machado.html).

## What is inside

This GitLab repository contains slide decks (see direcotry named `Slides`) and Google Colab notebooks (see directory named `Notebooks`) used in the course. The LaTeX code for producing the slide decks is available upon request. Some notebooks require input data that has been included in the `Notebooks` directory.

## Sharing matrials without permission is prohibited

Electronic video and/or audio recording was not permitted during class unless the student obtained permission from the instructor. If permission was granted, ***ANY*** distribution of the recording is prohibited. Students with specific electronic recording accommodations authorized by the Office of Disability Services do not require instructor permission; however, the instructor must be notified of any such accommodation before recording. Any distribution of such recordings is prohibited.

Copying and sharing exams, course notes, tests, lecture slides, assignments, or online content on any website, device, student groups, etc., **is prohibited** as this infringes on the professor’s rights and is a copyright infringement. Sharing any content without the instructor's explicit permission will result in an Academic Integrity Violation. The materials provided in here are for read-only purposes. **Please contact the author to ask for permission if you plan on reusing any of the materials in here**.

# Partial Copy of the Syllabus

The following is a partial copy of the syllabus of this couse, last updated on November 22, 2022.

## Course Description

"Machine learning" is a universally recognized term that usually refers to the science and engineering of building machines capable of doing various useful things without being explicitly programmed to do so. This course intends to provide enough information and training to get students a conformable understanding of the field and start asking the right questions when applying machine learning in the field of bioinformatics.

This course will review fundamental concepts from linear algebra and statistics to allow the student to appreciate the contents of this course thoroughly. Furthermore, the applications of these concepts to solve practical problems will be illustrated using examples.

### General aim

This 3-credit course aims to introduce commonly used machine learning techniques in bioinformatics.

Topics include dimension reduction using principal component analysis, singular value decomposition, and linear discriminant analysis. The course also introduces the students to clustering using k-means, hierarchical, expectation-maximization approaches, classification using k-nearest neighbor, and support vector machines.

To help understand these methods, basic concepts from linear algebra, optimization, and information theory are explained.

Applying these machine learning methods to solving bioinformatics problems is illustrated using examples from the literature.

## Course Content

This content will be explored in 29 classes of 75 minutes each. The remaining 4,575 minutes will be spent on out-of-class student work, reading days, and assessments. Note that this amount to approximately 9 hours of work per week if we sum up classes and out-of-class activities and account for days when there will be no classes.

The schedule below is subject to change and does not constitute a guaranteed list of all the topics covered in class.

### Topics for the first half term

These topics do not necessarily correspond to the content of each class. The speed at which we advance through the topics largely depends on how quickly the students absorb and learn new content.

Note that topic organization is heavily inspired by Burkov (2019) and that the students are incentivized to read it. The book title is "The Hundred-Page Machine Learning Book" (ISBN: 978-1-9995795-0-0; publisher: Andriy Burkov).

- Introduction to machine learning
    - What are artificial intelligence, machine learning, and deep learning
    - History of machine learning
    - How to represent knowledge using logical rules
    - The Turing test of artificial intelligence and how to interpret its results
- Types of machine learning:
    - Supervised machine learning
    - Unsupervised machine learning
    - Semi-supervised machine learning
    - Reinforcement machine learning
- More on supervised learning
    - How does supervised learning work
    - Why the model works on new data
- Notations and definitions—Part 1:
    - Data structures
    - Set and vector operations
    - Functions
    - Max and Arg Max
    - Assignment operator
    - Derivative and gradient
- Notations and definitions—Part 2:
    - Random variable
    - Unbiased estimators
    - Bayes' rule
    - Parameter estimation
    - Parameters vs. hyperparameters
- Notations and definitions—Part 3:
    - Classification vs. regression
    - Model-based vs. instance-based learning
    - Shallow vs. deep learning
- Fundamental algorithms—Part 1:
    - Linear regression
    - Logistic regression
- Fundamental algorithms—Part 2:
    - Decision tree learning
    - Support Vector Machine
    - Introduction to k-nearest neighbors algorithm (k-NN)
- Basic practice:
    - Learning algorithm selection
    - Three sets

### Topics for the second half term

- Artificial neural networks:
    - Multilayer perception example
    - Feed-forward neural network architecture
- Deep learning—Part 1:
    - Convolutional neural network
    - Recurrent neural network
- Deep learning as a tool for ecology and evolution—Part 1:
    - Theory
    - Example applications
- Deep learning as a tool for ecology and evolution—Part 2:
    - *Iris* identification based on flower measurements
    - Identification of jays from images
    - Distinguishing models of sequence evolution
- Deep learning applied for image identification and counting
- Problems and solutions—Part 1:
    - *k-means* clustering
    - Multiclass classification
    - One-class classification
    - Multi-Label Classification
- Problems and solutions—Part 2:
    - Ensemble machine learning
    - Learning to label sequences
    - Active learning
- Problems and solutions—Part 3:
    - Semi-supervised learning
    - One-shot learning
    - Zero-shot learning

## Recommended literature

New suggested references may be added in newer versions of the syllabus. The recommended literature corresponds to hand-picked documents indented to introduce the student to the science of machine learning applied to bioinformatics. Classes are heavily based on these materials.

- Bengio, Y., Goodfellow, I., & Courville, A. (2017). *Deep learning*. Publisher: MIT Press. ISBN: 9780262035613.
- Borowiec, M. L., Dikow, R. B., Frandsen, P. B., McKeeken, A., Valentini, G., & White, A. E. (2021). Deep learning as a tool for ecology and evolution. *Methods in Ecology and Evolution*. DOI: [https://doi.org/10.1111/2041-210X.13901](https://doi.org/10.1111/2041-210X.13901).
- Burkov, A. (2021) *The Hundred-Page Machine Learning Book*. Publisher: Andriy Burkov. ISBN: 9781999579500.
- Burger, S. V. (2018) *Introduction to Machine Learning with R: Rigorous Mathematical Analysis*. Publisher: O'Reilly. ISBN: 9781491976449.
- Chollet, F. (2021). *Deep learning with Python*. Publisher: Maning Publications Co. ISBN: 9781617296864.
- Russell, S. & Norving, P. (2021) Artificial intelligence: A modern approach (4th Edition). Publisher: Pearson. ISBN: 9780134610993.

Some additional recommended literature may be added to the course's files on CANVAS.
